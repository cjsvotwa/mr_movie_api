﻿using System.ComponentModel.DataAnnotations;

namespace MrMovieAPI.ViewModels
{
    public class MovieCountRequestViewModel
    {
        [Required(ErrorMessage = "Category is Required")]
        public int CategoryId { get; set; }
    }
}
