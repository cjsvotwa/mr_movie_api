﻿using System.ComponentModel.DataAnnotations;

namespace MrMovieAPI.ViewModels
{
    public class CategoryMasterViewModel
    {
        [Required(ErrorMessage = "Category Name is Required")]
        public string CategoryName { get; set; }

        public bool Status { get; set; }
    }
    public class CategoryMasterEditViewModel
    {
        [Required(ErrorMessage = "Category ID is Required")]
        public int CategoryID { get; set; }

        [Required(ErrorMessage = "Category Name is Required")]
        public string CategoryName { get; set; }

        public bool Status { get; set; }
    }


}
