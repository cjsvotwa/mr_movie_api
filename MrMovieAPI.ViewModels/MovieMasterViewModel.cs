﻿namespace MrMovieAPI.ViewModels
{
    public class MovieMasterViewModel
    {
        public int MovieID { get; set; }
        public string MovieName { get; set; }
        public int? CategoryID { get; set; }
        public int? Rating { get; set; }
    }

    public class MovieMasterDisplayViewModel
    {
        public int MovieID { get; set; }
        public string MovieName { get; set; }
        public int? CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int Rating { get; set; }
    }

    public class ActiveMovieModel
    {
        public string MovieID { get; set; }
        public string MovieName { get; set; }
        public int Rating { get; set; }
        public int Count { get; set; }
    }
}
