﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;

namespace MrMovieAPI.Concrete
{
    public class CategoryMasterConcrete : ICategoryMaster
    {
        private readonly DatabaseContext _context;
        private readonly IConfiguration _configuration;
        public CategoryMasterConcrete( DatabaseContext context, IConfiguration config )
        {
            _context = context;
            _configuration = config;
        }
        public List<CategoryMaster> GetCategoryMasterList()
        {
            try
            {
                var result = ( from category in _context.CategoryMaster
                               select category ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetCategoryMasterList", _Ex.Message );
            }
            return null;
        }

        public CategoryMaster GetCategoryMasterbyId( int categoryId )
        {
            try
            {
                var result = ( from category in _context.CategoryMaster
                               where category.CategoryID == categoryId
                               select category ).FirstOrDefault();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetCategoryMasterbyId", _Ex.Message );
            }
            return null;
        }

        public bool CheckCategoryNameExists( string categoryName, int id = 0 )
        {
            try
            {
                var result = ( from category in _context.CategoryMaster
                               where ( category.CategoryName == categoryName
                               && category.CategoryID != id )
                               select category ).Count();
                return ( result > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckCategoryNameExists", _Ex.Message );
            }
            return false;
        }

        public bool AddCategoryMaster( CategoryMaster categoryMaster )
        {
            try
            {
                _context.CategoryMaster.Add( categoryMaster );
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "AddCategoryMaster", _Ex.Message );
            }
            return false;
        }

        public bool UpdateCategoryMaster( CategoryMaster categoryMaster )
        {
            try
            {
                _context.Entry( categoryMaster ).Property( x => x.CategoryName ).IsModified = true;
                _context.Entry( categoryMaster ).Property( x => x.Status ).IsModified = true;
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "UpdateCategoryMaster", _Ex.Message );
            }
            return false;
        }

        public bool DeleteCategory( int categoryId )
        {
            try
            {
                var categorymaster = ( from category in _context.CategoryMaster
                                       where category.CategoryID == categoryId
                                       select category ).FirstOrDefault();
                if ( categorymaster != null )
                {
                    _context.CategoryMaster.Remove( categorymaster );
                    return ( _context.SaveChanges() > 0 );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "DeleteCategory", _Ex.Message );
            }
            return false;
        }

        public List<CategoryMaster> GetActiveCategoryMasterList()
        {
            try
            {
                var result = ( from category in _context.CategoryMaster
                               where category.Status == true
                               select category ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetActiveCategoryMasterList", _Ex.Message );
            }
            return null;
        }
    }
}
