﻿using Microsoft.EntityFrameworkCore;
using MrMovieAPI.Models;

namespace MrMovieAPI.Concrete
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext( DbContextOptions<DatabaseContext> options ) : base( options )
        {

        }
        public DbSet<CategoryMaster> CategoryMaster { get; set; }
        public DbSet<MovieMaster> MovieMaster { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Users> Users { get; set; }
    }
}
