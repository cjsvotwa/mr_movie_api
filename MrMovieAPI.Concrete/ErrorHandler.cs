﻿using System;
using System.IO;

namespace MrMovieAPI.Concrete
{
    class ErrorHandler
    {
        private const string LOG_FILE_NAME = "error_log.txt";
        public static void handleError( ElementType _ElementType, string _sElementName, string _sMethodName, string _sMessage )
        {
            try
            {
                _sMessage = DateTime.Now.ToString() +
                            @"Element type: " + _ElementType.ToString() +  //if to db we use int
                             " Element name: " + _sElementName +
                             " Method name: " + _sMethodName +
                             " Error message: " + _sMessage
                            + Environment.NewLine;
                logError( _sMessage );
            }
            catch ( Exception )
            {
                //Fail silently
            }
        }

        private static void logError( string _sMessage )
        {
            try
            {
                //could log the error to a database
                string _sFile = AppDomain.CurrentDomain.BaseDirectory + LOG_FILE_NAME;
                using ( StreamWriter _StreamWriter = File.AppendText( _sFile ) )
                {
                    _StreamWriter.WriteLine( _sMessage );
                }
            }
            catch ( Exception )
            {
                //Fail silently
            }
        }
    }
}
