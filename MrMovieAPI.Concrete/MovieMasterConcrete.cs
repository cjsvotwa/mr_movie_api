﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Microsoft.Extensions.Configuration;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Concrete
{
    public class MovieMasterConcrete : IMovieMaster
    {
        private readonly IConfiguration _configuration;
        private readonly DatabaseContext _context;
        public MovieMasterConcrete( DatabaseContext context, IConfiguration configuration )
        {
            _context = context;
            _configuration = configuration;

        }
        public void InsertMovie( MovieMaster movie )
        {
            try
            {
                using ( SqlConnection con = new SqlConnection( _configuration.GetConnectionString( "DatabaseConnection" ) ) )
                {
                    var paramater = new DynamicParameters();
                    paramater.Add( "@MovieID", movie.MovieID );
                    paramater.Add( "@MovieName", movie.MovieName );
                    paramater.Add( "@CategoryID", movie.CategoryID );
                    paramater.Add( "@Rating", movie.Rating );
                    paramater.Add( "@CreateUserID", movie.CreateUserID );
                    var value = con.Query<int>( "sp_save_movie", paramater, null, true, 0, commandType: CommandType.StoredProcedure );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "InsertMovie", _Ex.Message );
            }
        }

        public bool CheckMovieExits( string movieName, int id = 0 )
        {
            try
            {
                var result = ( from movie in _context.MovieMaster
                               where ( movie.MovieName == movieName
                               && movie.MovieID != id )
                               select movie ).Count();

                return result > 0 ? true : false;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckMovieExits", _Ex.Message );
            }
            return false;
        }

        public List<MovieMasterDisplayViewModel> GetMovieMasterList()
        {
            try
            {
                var result = ( from movie in _context.MovieMaster
                               join category in _context.CategoryMaster on movie.CategoryID equals category.CategoryID
                               select new MovieMasterDisplayViewModel
                               {
                                   MovieID = movie.MovieID,
                                   MovieName = movie.MovieName,
                                   CategoryID = category.CategoryID,
                                   CategoryName = category.CategoryName,
                                   Rating = movie.Rating
                               } ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetMovieMasterList", _Ex.Message );
            }
            return null;
        }

        public List<ActiveMovieModel> GetActiveMovieMasterList( int? categoryId )
        {
            try
            {
                var result = ( from movie in _context.MovieMaster
                               where movie.CategoryID == categoryId
                               select new ActiveMovieModel
                               {
                                   MovieName = movie.MovieName,
                                   MovieID = movie.MovieID.ToString()
                               } ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetActiveMovieMasterList", _Ex.Message );
            }
            return null;
        }

        public List<ActiveMovieModel> GetRatingCategorisedMovieMasterList( int minRating = 0, int maxRating = 5 )
        {
            try
            {
                if ( minRating > maxRating || ( minRating < 0 || maxRating < 0 ) )
                {
                    minRating = 0;
                    maxRating = 5;
                }
                ActiveMovieModel model;
                List<ActiveMovieModel> models = new List<ActiveMovieModel>();
                while ( minRating <= maxRating )
                {
                    model = new ActiveMovieModel
                    {
                        Rating = minRating,
                        Count = ( ( from movie in _context.MovieMaster
                                    where movie.Rating == minRating
                                    select new ActiveMovieModel
                                    {
                                        MovieName = movie.MovieName,
                                        MovieID = movie.MovieID.ToString()
                                    } ).Count() )
                    };
                    models.Add( model );
                    minRating++;
                }
                return models;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetCategorisedMovieMasterList", _Ex.Message );
            }
            return null;
        }

        public bool UpdateMovieMaster( MovieMaster movieMaster )
        {
            try
            {
                _context.Entry( movieMaster ).Property( x => x.MovieName ).IsModified = true;
                _context.Entry( movieMaster ).Property( x => x.CategoryID ).IsModified = true;
                _context.Entry( movieMaster ).Property( x => x.Rating ).IsModified = true;
                _context.Entry( movieMaster ).Property( x => x.CreateUserID ).IsModified = true;
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "UpdateMovieMaster", _Ex.Message );
            }
            return false;
        }

        public bool DeleteMovie( int movieId )
        {
            try
            {
                var moviemaster = ( from movie in _context.MovieMaster
                                    where movie.MovieID == movieId
                                    select movie ).FirstOrDefault();
                if ( moviemaster != null )
                {
                    _context.MovieMaster.Remove( moviemaster );
                    return ( _context.SaveChanges() > 0 );
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "DeleteMovie", _Ex.Message );
            }
            return false;
        }

        public MovieMasterViewModel GetMovieMasterbyId( int movieId )
        {
            try
            {
                var result = ( from movie in _context.MovieMaster
                               where movie.MovieID == movieId
                               select new MovieMasterViewModel
                               {
                                   MovieID = movie.MovieID,
                                   MovieName = movie.MovieName,
                                   CategoryID = movie.CategoryID,
                                   Rating = movie.Rating
                               } ).FirstOrDefault();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetMovieMasterbyId", _Ex.Message );
            }
            return null;
        }
    }
}
