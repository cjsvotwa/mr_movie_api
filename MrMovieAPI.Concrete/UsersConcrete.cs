﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Concrete
{
    public class UsersConcrete : IUsers
    {

        private readonly DatabaseContext _context;
        public UsersConcrete( DatabaseContext context )
        {
            _context = context;
        }

        public bool CheckUsersExits( string username )
        {
            try
            {
                return ( ( from user in _context.Users
                           where user.UserName == username
                           select user ).Count() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "CheckUsersExits", _Ex.Message );
            }
            return false;
        }

        public bool AuthenticateUsers( string username, string password )
        {
            try
            {
                return ( ( from user in _context.Users
                           where user.UserName == username && user.Password == password
                           select user ).Count() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "AuthenticateUsers", _Ex.Message );
            }
            return false;
        }

        public LoginResponse GetUserDetailsbyCredentials( string username )
        {
            try
            {
                var result = ( from user in _context.Users
                               where user.UserName == username
                               select new LoginResponse
                               {
                                   UserId = user.UserId,
                                   RoleId = 0,
                                   Status = user.Status,
                                   UserName = user.UserName
                               } ).SingleOrDefault();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUserDetailsbyCredentials", _Ex.Message );
            }
            return null;
        }

        public bool DeleteUsers( int userId )
        {
            try
            {
                var removeuser = ( from user in _context.Users
                                   where user.UserId == userId
                                   select user ).FirstOrDefault();
                if ( removeuser == null )
                {
                    return false;
                }
                _context.Users.Remove( removeuser );
                return ( _context.SaveChanges() > 0 );
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "DeleteUsers", _Ex.Message );
            }
            return false;
        }

        public List<Users> GetAllUsers()
        {
            try
            {
                var result = ( from user in _context.Users
                               where user.Status == true
                               select user ).ToList();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUsersbyId", _Ex.Message );
            }
            return null;
        }

        public Users GetUsersbyId( int userId )
        {
            try
            {
                var result = ( from user in _context.Users
                               where user.UserId == userId
                               select user ).FirstOrDefault();
                return result;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( ElementType._class, this.GetType().Name, "GetUsersbyId", _Ex.Message );
            }
            return null;
        }
    }
}
