﻿using System.Collections.Generic;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Interface
{
    public interface IMovieMaster
    {
        void InsertMovie( MovieMaster movie );
        bool CheckMovieExits( string movieName, int id = 0 );
        List<MovieMasterDisplayViewModel> GetMovieMasterList();
        MovieMasterViewModel GetMovieMasterbyId( int movieId );
        bool DeleteMovie( int movieId );
        bool UpdateMovieMaster( MovieMaster movieMaster );
        List<ActiveMovieModel> GetActiveMovieMasterList( int? categoryId );
        List<ActiveMovieModel> GetRatingCategorisedMovieMasterList( int minRating = 0, int maxRating = 5 );
    }
}