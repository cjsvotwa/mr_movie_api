﻿using System.Collections.Generic;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Interface
{
    public interface IUsers
    {
        bool CheckUsersExits( string username );
        Users GetUsersbyId( int userid );
        List<Users> GetAllUsers();
        bool AuthenticateUsers( string username, string password );
        LoginResponse GetUserDetailsbyCredentials( string username );
    }
}