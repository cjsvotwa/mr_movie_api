﻿using System.Collections.Generic;
using MrMovieAPI.Models;

namespace MrMovieAPI.Interface
{
    public interface ICategoryMaster
    {
        bool AddCategoryMaster( CategoryMaster categoryMaster );
        List<CategoryMaster> GetCategoryMasterList();
        CategoryMaster GetCategoryMasterbyId( int categoryId );
        bool CheckCategoryNameExists( string categoryName, int id = 0 );
        bool UpdateCategoryMaster( CategoryMaster categoryMaster );
        bool DeleteCategory( int categoryId );
        List<CategoryMaster> GetActiveCategoryMasterList();
    }
}
