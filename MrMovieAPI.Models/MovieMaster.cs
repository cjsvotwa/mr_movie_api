﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MrMovieAPI.Models
{
    public class MovieMaster
    {
        [Key]
        public int MovieID { get; set; }
        public string MovieName { get; set; }
        public int CategoryID { get; set; }
        public int Rating { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? CreateUserID { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? ModifyUserID { get; set; }
    }

}
