﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MrMovieAPI.Models
{
    public class CategoryMaster
    {
        [Key]
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int Createdby { get; set; }
        public DateTime Createddate { get; set; }
        public bool Status { get; set; }      
    }
}
