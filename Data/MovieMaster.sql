USE [master]
GO
/****** Object:  Database [MovieMaster]    Script Date: 2019-05-29 18:00:38 ******/
CREATE DATABASE [MovieMaster]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MovieMaster', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\MovieMaster.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'MovieMaster_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\MovieMaster_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [MovieMaster] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MovieMaster].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MovieMaster] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MovieMaster] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MovieMaster] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MovieMaster] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MovieMaster] SET ARITHABORT OFF 
GO
ALTER DATABASE [MovieMaster] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MovieMaster] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MovieMaster] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MovieMaster] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MovieMaster] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MovieMaster] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MovieMaster] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MovieMaster] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MovieMaster] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MovieMaster] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MovieMaster] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MovieMaster] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MovieMaster] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MovieMaster] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MovieMaster] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MovieMaster] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MovieMaster] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MovieMaster] SET RECOVERY FULL 
GO
ALTER DATABASE [MovieMaster] SET  MULTI_USER 
GO
ALTER DATABASE [MovieMaster] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MovieMaster] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MovieMaster] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MovieMaster] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MovieMaster] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MovieMaster', N'ON'
GO
USE [MovieMaster]
GO
/****** Object:  Table [dbo].[CategoryMaster]    Script Date: 2019-05-29 18:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CategoryMaster](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](100) NULL,
	[Createdby] [int] NULL,
	[Createddate] [datetime] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_SchemeMaster] PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MovieMaster]    Script Date: 2019-05-29 18:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MovieMaster](
	[MovieID] [int] IDENTITY(1,1) NOT NULL,
	[MovieName] [varchar](100) NULL,
	[CategoryID] [int] NULL,
	[Rating] [int] NULL,
	[CreateDate] [datetime] NULL,
	[CreateUserID] [int] NULL,
	[ModifyDate] [datetime] NULL,
	[ModifyUserID] [int] NULL,
 CONSTRAINT [PK_PlanMaster] PRIMARY KEY CLUSTERED 
(
	[MovieID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2019-05-29 18:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](56) NOT NULL,
	[FullName] [nvarchar](200) NULL,
	[EmailId] [nvarchar](200) NULL,
	[Contactno] [nvarchar](10) NULL,
	[Password] [nvarchar](200) NULL,
	[Createdby] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK__Users__3214EC070F975522] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CategoryMaster] ON 

INSERT [dbo].[CategoryMaster] ([CategoryID], [CategoryName], [Createdby], [Createddate], [Status]) VALUES (23, N'Test Category', 1, CAST(N'2019-05-29 06:37:36.607' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[CategoryMaster] OFF
SET IDENTITY_INSERT [dbo].[MovieMaster] ON 

INSERT [dbo].[MovieMaster] ([MovieID], [MovieName], [CategoryID], [Rating], [CreateDate], [CreateUserID], [ModifyDate], [ModifyUserID]) VALUES (17, N'Test Movie', 23, 3, CAST(N'2019-05-29 17:40:21.557' AS DateTime), 1, CAST(N'2019-05-29 17:40:21.557' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[MovieMaster] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([UserId], [UserName], [FullName], [EmailId], [Contactno], [Password], [Createdby], [CreatedDate], [Status]) VALUES (1, N'Admin', N'Admin', N'admin@admin.com', N'0612080529', N'tttdoybuFsAnWJYAfwOUqg==', 1, CAST(N'2019-05-29 13:43:08.607' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  StoredProcedure [dbo].[sp_save_movie]    Script Date: 2019-05-29 18:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_save_movie]
(
	@MovieID int = 0 output,
	@MovieName nvarchar(100) = null,
	@CategoryID int = NULL,
	@Rating SMALLINT,
	@CreateUserID INT = 0
)
AS

BEGIN
	DECLARE @ReturnValue INT
	IF @MovieID IS NULL OR @MovieID = 0 OR NOT exists(select * from MovieMaster where MovieID = MovieID)
		BEGIN
			INSERT INTO MovieMaster( MovieName, CategoryID, Rating, CreateDate, CreateUserID,
			 ModifyDate, ModifyUserID)
			VALUES ( @MovieName, @CategoryID, @Rating, getdate(), @CreateUserID,
			 getdate(), @CreateUserID)
			 
			 SELECT @ReturnValue = SCOPE_IDENTITY()		  
		END
	ELSE
		BEGIN
			UPDATE MovieMaster SET
				MovieName = @MovieName,
				CategoryID = @CategoryID,
				Rating = @Rating,
				ModifyDate = getdate(),				
				ModifyUserID = @CreateUserID
			WHERE MovieID = @MovieID

			SELECT @ReturnValue = @MovieID
		END
END
IF (@@ERROR != 0)
BEGIN
	RETURN - 1
END
ELSE
BEGIN
	RETURN @ReturnValue
END

GO
USE [master]
GO
ALTER DATABASE [MovieMaster] SET  READ_WRITE 
GO
