﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CategoryMasterViewModel, CategoryMaster>()
                .ForMember( dest => dest.CategoryName, opt => opt.MapFrom( src => src.CategoryName ) )
                .ForMember( dest => dest.Status, opt => opt.MapFrom( src => src.Status ) );

            CreateMap<CategoryMasterEditViewModel, CategoryMaster>()
                .ForMember( dest => dest.CategoryName, opt => opt.MapFrom( src => src.CategoryName ) )
                .ForMember( dest => dest.CategoryID, opt => opt.MapFrom( src => src.CategoryID ) )
                .ForMember( dest => dest.Status, opt => opt.MapFrom( src => src.Status ) );

            CreateMap<MovieMasterViewModel, MovieMaster>()
                .ForMember( dest => dest.MovieID, opt => opt.MapFrom( src => src.MovieID ) )
                .ForMember( dest => dest.CategoryID, opt => opt.MapFrom( src => src.CategoryID ) )
                .ForMember( dest => dest.MovieName, opt => opt.MapFrom( src => src.MovieName ) )
                .ForMember( dest => dest.Rating, opt => opt.MapFrom( src => src.Rating ) );
        }
    }
}
