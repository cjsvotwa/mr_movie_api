﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MrMovieAPI.Common;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryMaster _categoryMaster;
        public CategoryController( ICategoryMaster categoryMaster )
        {
            _categoryMaster = categoryMaster;
        }

        // GET: api/Category
        [HttpGet]
        public List<CategoryMaster> Get()
        {
            return _categoryMaster.GetCategoryMasterList();
        }

        // GET: api/Category/5
        [HttpGet( "{id}", Name = "GetCategory" )]
        public CategoryMaster Get( int id )
        {
            return _categoryMaster.GetCategoryMasterbyId( id );
        }

        // POST: api/Category
        [HttpPost]
        public HttpResponseMessage Post( [FromBody] CategoryMasterViewModel categoryMaster )
        {
            try
            {
                if ( ModelState.IsValid )
                {
                    if ( _categoryMaster.CheckCategoryNameExists( categoryMaster.CategoryName ) )
                    {
                        var response = new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.Conflict
                        };
                        return response;
                    }
                    else
                    {
                        var userId = this.User.FindFirstValue( ClaimTypes.Name );
                        var tempCategoryMaster = AutoMapper.Mapper.Map<CategoryMaster>( categoryMaster );
                        tempCategoryMaster.Createddate = DateTime.Now;
                        tempCategoryMaster.Createdby = Convert.ToInt32( userId );
                        _categoryMaster.AddCategoryMaster( tempCategoryMaster );
                        var response = new HttpResponseMessage()
                        {
                            StatusCode = HttpStatusCode.OK
                        };
                        return response;
                    }
                }
                else
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                    return response;
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
            }
            return null;
        }

        // PUT: api/Category/5
        [HttpPut( "{id}" )]
        public HttpResponseMessage Put( int id, [FromBody] CategoryMasterEditViewModel categoryMaster )
        {
            try
            {
                if ( string.IsNullOrWhiteSpace( Convert.ToString( id ) ) || categoryMaster == null )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.BadRequest
                    };
                    return response;
                }
                if ( _categoryMaster.CheckCategoryNameExists( categoryMaster.CategoryName, categoryMaster.CategoryID ) )
                {
                    var _response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return _response;
                }
                var temp = AutoMapper.Mapper.Map<CategoryMaster>( categoryMaster );
                var result = _categoryMaster.UpdateCategoryMaster( temp );
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Put", _Ex.Message );
            }
            return null;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete( "{id}" )]
        public HttpResponseMessage Delete( int id )
        {
            try
            {
                var result = _categoryMaster.DeleteCategory( id );
                if ( result )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK
                    };
                    return response;
                }
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.BadRequest
                };
                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Delete", _Ex.Message );
            }
            return null;
        }
    }
}
