﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MrMovieAPI.Common;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class MovieMasterController : ControllerBase
    {
        private readonly IMovieMaster _movieMaster;
        public MovieMasterController( IMovieMaster movieMaster )
        {
            _movieMaster = movieMaster;
        }
        // GET: api/MovieMaster
        [HttpGet]
        public IEnumerable<MovieMasterDisplayViewModel> Get()
        {
            return _movieMaster.GetMovieMasterList();
        }

        // GET: api/MovieMaster/5
        [HttpGet( "{id}", Name = "GetMovie" )]
        public MovieMasterViewModel Get( int id )
        {
            try
            {
                return _movieMaster.GetMovieMasterbyId( id );
            }
            catch ( Exception )
            {
                throw;
            }
        }

        // POST: api/MovieMaster
        [HttpPost]
        public HttpResponseMessage Post( [FromBody] MovieMasterViewModel movieMasterViewModel )
        {
            try
            {
                if ( _movieMaster.CheckMovieExits( movieMasterViewModel.MovieName ) )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return response;
                }
                else
                {
                    var userId = this.User.FindFirstValue( ClaimTypes.Name );
                    var tempmovieMaster = AutoMapper.Mapper.Map<MovieMaster>( movieMasterViewModel );
                    tempmovieMaster.CreateUserID = Convert.ToInt32( userId );
                    _movieMaster.InsertMovie( tempmovieMaster );

                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK
                    };
                    return response;
                }
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return response;
            }
        }

        // PUT: api/MovieMaster/5
        [HttpPut( "{id}" )]
        public HttpResponseMessage Put( int id, [FromBody] MovieMasterViewModel movieMasterViewModel )
        {
            try
            {
                if ( _movieMaster.CheckMovieExits( movieMasterViewModel.MovieName, movieMasterViewModel.MovieID ) )
                {
                    var response = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.Conflict
                    };
                    return response;
                }
                var userId = this.User.FindFirstValue( ClaimTypes.Name );
                var tempmovieMaster = AutoMapper.Mapper.Map<MovieMaster>( movieMasterViewModel );
                tempmovieMaster.CreateUserID = Convert.ToInt32( userId );
                _movieMaster.UpdateMovieMaster( tempmovieMaster );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };

                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Post", _Ex.Message );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return _response;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete( "{id}" )]
        public HttpResponseMessage Delete( int id )
        {
            try
            {
                _movieMaster.DeleteMovie( id );
                var _response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.OK
                };
                return _response;
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Delete", _Ex.Message );
                var response = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return response;
            }
        }
    }
}
