﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MrMovieAPI.Common;
using MrMovieAPI.Interface;
using MrMovieAPI.ViewModels;

namespace MrMovieAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class GetRatingCategoryController : ControllerBase
    {
        private readonly IMovieMaster _movieMaster;
        public GetRatingCategoryController( IMovieMaster movieMaster )
        {
            _movieMaster = movieMaster;
        }
        // GET: api/MovieMaster
        [HttpGet]
        public List<ActiveMovieModel> Get()
        {
            return _movieMaster.GetRatingCategorisedMovieMasterList();
        }
    }
}
