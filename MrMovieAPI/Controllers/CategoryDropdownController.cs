﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MrMovieAPI.Common;
using MrMovieAPI.Interface;
using MrMovieAPI.Models;

namespace MrMovieAPI.Controllers
{
    [Authorize]
    [Route( "api/[controller]" )]
    [ApiController]
    public class CategoryDropdownController : ControllerBase
    {
        private readonly ICategoryMaster _categoryMaster;
        public CategoryDropdownController( ICategoryMaster categoryMaster )
        {
            _categoryMaster = categoryMaster;
        }
        // GET: api/CategoryDropdown
        [HttpGet]
        public IEnumerable<CategoryMaster> Get()
        {
            try
            {
                return _categoryMaster.GetActiveCategoryMasterList();
            }
            catch ( Exception _Ex )
            {
                ErrorHandler.handleError( "Controller", this.GetType().Name, "Get", _Ex.Message );
            }
            return null;
        }


    }
}
